import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:blog/styles/colors.dart';
import 'package:blog/styles/text_style.dart';

enum AppTheme { Light, Dark }

final appThemeData = {
  AppTheme.Light: ThemeData(
    brightness: Brightness.light,
    primaryColor: CommonColor.colorGreen,
    primaryColorDark: CommonColor.colorGreenDark,
    scaffoldBackgroundColor: CommonColor.colorWhiteBg,
    accentColor: CommonColor.colorYellow,
    cursorColor: CommonColor.colorYellowDark,
    cardColor: CommonColor.colorWhiteCard,
    errorColor: CommonColor.colorRed,
    primaryColorLight: Colors.grey[300],
    backgroundColor: CommonColor.colorWhiteBg,
    shadowColor: CommonColor.colorGreyPastel,
    fontFamily: 'Helvetica',
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    hoverColor:Colors.transparent,
    dividerColor: CommonColor.colorGreyPastel,
    canvasColor: CommonColor.colorBlack,
    hintColor: CommonColor.colorGreyPastel,
    //primarySwatch: CommonColor.colorGreen, => need MaterialColor
    primarySwatch: MaterialColors.colorLight,
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: CommonColor.colorWhiteCard,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20),
          topLeft: Radius.circular(20),
        ),
      )
    ),
    buttonTheme: ButtonThemeData(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25),
      ),
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 20,),
      buttonColor: CommonColor.colorGreen,
      splashColor: CommonColor.colorGreenDark,
      disabledColor: CommonColor.colorGrey,
      textTheme: ButtonTextTheme.normal,
    ),
    dialogTheme: DialogTheme(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      elevation: 6,
      backgroundColor: CommonColor.colorWhiteCard,
    ),
    dialogBackgroundColor: CommonColor.colorWhiteCard,
    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      bodyText1: CommonTextStyle.size16Light,
      bodyText2: CommonTextStyle.size16Light,
      button: CommonTextStyle.size16Medium.copyWith(color: CommonColor.colorWhiteCard,),
    ),
  ),
  AppTheme.Dark: ThemeData(
    // primaryColorDark: Colors.white60,
    primaryColorDark: Colors.grey[300],
    scaffoldBackgroundColor: Colors.purpleAccent,
    brightness: Brightness.dark,
    primaryColor: Colors.grey[300],
    accentColor: Colors.amber[700],
    primaryColorLight: Colors.white60,
    backgroundColor: Colors.black,
    fontFamily: 'Helvetica',
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    hoverColor:Colors.transparent,
    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      headline1: TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold),
      headline2: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold),
      headline6: TextStyle(fontSize: 20.0, fontStyle: FontStyle.italic),
      bodyText1: TextStyle(color: Colors.white60, fontWeight: FontWeight.normal, fontSize: 12),
      bodyText2: TextStyle(color: Colors.white60, fontWeight: FontWeight.normal, fontSize: 14),
    ),
  ),
};