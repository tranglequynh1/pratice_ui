import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blog/config/theme.dart';

class ThemesManager extends ChangeNotifier {

  ThemeData _themeData;
  ThemeData get themeData => _themeData;

  ThemesManager() {
    _themeData = appThemeData[AppTheme.Light];
  }

  void changeThemes(AppTheme appTheme) {
    _themeData = appThemeData[appTheme];
    notifyListeners();
  }

}
