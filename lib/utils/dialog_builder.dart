import 'package:blog/plugin/locator.dart';
import 'package:blog/plugin/navigator.dart';
import 'package:blog/styles/text_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

enum DialogType { NONE, YES_NO, CONFIRM_CANCEL, CANCEL }

final currentContext = locator<NavigatorService>().navigatorKey.currentState.overlay.context;

abstract class DialogBuilder {

  static final _theme = Theme.of(currentContext);


  static Future<bool> buildNotificationDialog({ String urlImage, DialogType dialogType = DialogType.NONE, VoidCallback function, bool barrierDismissible = false, Widget content, String message }) async {

    List<Widget> _listActions = [];

    switch (dialogType) {
      case DialogType.NONE:
        break;
      case DialogType.CANCEL:
        _listActions.add(
          buildButton(
            title: 'Cancel',
            textColor: Theme.of(currentContext).accentColor,
            onPress: () {
              Navigator.of(currentContext).pop(false);
            },
          )
        );
        break;
      case DialogType.YES_NO:
        _listActions.add(
          buildButton(
            title: 'Yes',
            textColor: Theme.of(currentContext).primaryColor,
            onPress: function ?? () {
              Navigator.of(currentContext).pop(true);
            },
          )
        );
        _listActions.add(
          buildButton(
          title: 'No',
          textColor: Theme.of(currentContext).cursorColor, onPress: () {
            Navigator.of(currentContext).pop(false);
          }),
        );
        break;
      case DialogType.CONFIRM_CANCEL:
        _listActions.add(
          buildButton(
            title: 'Confirm',
            textColor: Theme.of(currentContext).primaryColor,
            onPress: function ?? () {
              Navigator.of(currentContext).pop(true);
            },
          ),
        );
        _listActions.add(
          buildButton(
            title: 'Cancel',
            textColor: Theme.of(currentContext).cursorColor,
            onPress: () {
              Navigator.of(currentContext).pop(false);
            },
          ),
        );
        break;
      default:
        break;
    }

    Widget _content = Padding(
      child: Column(
        children: [
          SizedBox(
            height: 46,
            width: 46,
            child: SvgPicture.asset(
              urlImage ?? 'assets/images/system/ic_communication.svg',
              fit: BoxFit.contain,
            ),
          ),
          const SizedBox(height: 20,),
          Text(
            message ?? '',
            style: CommonTextStyle.size16Medium,
          ),
          content ?? const SizedBox(),
        ],
      ),
      padding: const EdgeInsets.symmetric(vertical: 15,),
    );

    CupertinoAlertDialog alertDialog = new CupertinoAlertDialog(
      actions: _listActions,
      content: _content,
    );

    bool result = await showDialog(
      context: currentContext,
      barrierDismissible: barrierDismissible,
      builder: (_) {
        if (dialogType == DialogType.NONE && !barrierDismissible) {
          Future.delayed(Duration(milliseconds: 3500)).then((value) {
            Navigator.of(currentContext).pop(false);
          });
        }
        return WillPopScope(
          child: alertDialog,
          onWillPop: () async => barrierDismissible,
        );
      }
    );
    return result;
  }

  static void buildLoadingDialog() {
    showDialog(
      context: currentContext,
      barrierDismissible: false,
      barrierColor: Theme.of(currentContext).disabledColor.withOpacity(0.3),
      builder: (_) {
        return WillPopScope(
          child: Center(
            child: SizedBox(
              height: 32,
              width: 32,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                backgroundColor: Colors.white.withOpacity(0.03),
              ),
            ),
          ),
          onWillPop: () async => false
        );
      }
    );
  }

}

Widget buildButton({ VoidCallback onPress, @required String title, Color textColor, }) {
  return FlatButton(
    onPressed: onPress,
    splashColor: Theme.of(currentContext).hintColor,
    padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10,),
    child: Text(
      title ?? '',
      style: CommonTextStyle.size16Bold.copyWith(
        color: textColor ?? Theme.of(currentContext).primaryColor,
      ),
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(0)),
    ),
  );
}