import 'package:blog/styles/text_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class AppBarBuilder {

  static AppBar primaryAppBar(BuildContext context, { String title, Color backgroundColor, List<Widget> actions, Color textColor, bool centerTitle, Widget leading, double elevation = 1, List action}) {
    return AppBar(
      elevation: elevation,
      toolbarHeight: 46,
      leadingWidth: 46,
      shadowColor: elevation != 0 ? Colors.grey[100] : null,
      leading: leading ?? InkWell(
        child: Container(
          color: Colors.transparent,
          child: Icon(Icons.arrow_back, size: 26, color: Theme.of(context).primaryColor,),
          padding: const EdgeInsets.only(right: 6, left: 20,),
        ),
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
      actions: action,
      title: Text(title ?? '', style: CommonTextStyle.size20Medium.copyWith(color: textColor),),
      centerTitle: centerTitle ?? true,
      backgroundColor: backgroundColor ?? Theme.of(context).cardColor,
    );
  }

}
