import 'package:blog/router/routing_name.dart';
import 'package:blog/widgets/backgrounds/background.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blog/styles/text_style.dart';
import 'package:blog/widgets/buttons/flat_button.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class SplashScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }

}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {

  AnimationController _animationController;
  Animation<double> _animation;
  bool isReversing = false;
  int _currentPage = 0;
  final Duration _duration = Duration(milliseconds: 1200,);
  PageController _pageController = new PageController(initialPage: 0, );
  List<String> _liImage = [
    'assets/images/templates/page1.png',
    'assets/images/templates/page2.png',
    'assets/images/templates/page3.png',
  ];

  @override
  void initState() {
    super.initState();
    _animationController = new AnimationController(
      vsync: this,
      duration: _duration,
    );
    _animation = Tween(begin: 0.0, end: 1.0).animate(_animationController);

    _animationController.addListener(() async {
      if (_animationController.status == AnimationStatus.completed) {
        _animationController.reset(); //Reset the controller
        await Future.delayed(Duration(milliseconds: 460));
        if (isReversing && _currentPage > 0) {
          _currentPage--;
        } else if (isReversing && _currentPage == 0) {
          _currentPage++;
          isReversing = false;
        } else if (_currentPage < _liImage.length - 1) {
          _currentPage++;
        } else {
          _currentPage--;
          isReversing = true;
        }
        _pageController.animateToPage(_currentPage, duration: _duration, curve: Curves.linear);
      }
    });
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          const Positioned(child: Background(), right: 0,),
          _buildBody()
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 60,),
          Expanded(
            child: _buildPageView(),
          ),
          const SizedBox(height: 20,),
          _buildIndicatorSmooth(),
          const SizedBox(height: 20,),
          _buildButtonToLogin(),
          const SizedBox(height: 12,),
          _buildButtonOthers(),
          const SizedBox(height: 60,),
        ],
      ),
    );
  }

  Widget _buildPageView() {
    return PageView.builder(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      onPageChanged: (index) {
        _animationController.forward();
      },
      allowImplicitScrolling: true,
      itemBuilder: (BuildContext context, int index) {
        return SizedBox(
          child: Image.asset(_liImage[index], fit: BoxFit.contain,),
          width: double.infinity - 38,
        );
      },
      itemCount: _liImage?.length ?? 0,
      reverse: false,
      scrollDirection: Axis.horizontal,
    );
  }

  Widget _buildIndicatorSmooth() {
    return SmoothPageIndicator(
      controller: _pageController,
      count: _liImage?.length ?? 0,
      axisDirection: Axis.horizontal,
      effect: WormEffect(
        dotColor: Theme.of(context).disabledColor.withOpacity(0.1),
        activeDotColor: Theme.of(context).accentColor,
        paintStyle: PaintingStyle.stroke,
        strokeWidth: 1.2,
        dotHeight: 10,
        dotWidth: 10,
        radius: 10,
      ),
    );
  }

  Widget _buildButtonToLogin() {
    return RaisedButton(
      onPressed: () {
        Navigator.of(context).pushNamed(RoutingNameConstant.loginScreen,);
      },
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Text(
          'Login my account',
          textAlign: TextAlign.center,
          style: CommonTextStyle.size16Medium.copyWith(color: Theme.of(context).cardColor,),
        ),
      ),
    );
  }

  Widget _buildButtonOthers() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: flatButton(
            title: 'Facebook',
            image: 'assets/images/icons/ic_facebook.svg',
            textColor: Theme.of(context).canvasColor.withOpacity(0.6),
            borderColor: Theme.of(context).disabledColor,
            fillColor: Theme.of(context).backgroundColor,
            onPress: () {
              print('click Facebook');
            }
          ),
        ),
        const SizedBox(width: 12,),
        Expanded(
          flex: 1,
          child: flatButton(
            title: 'Google',
            image: 'assets/images/icons/ic_google.svg',
            textColor: Theme.of(context).canvasColor.withOpacity(0.6),
            borderColor: Theme.of(context).disabledColor,
            fillColor: Theme.of(context).backgroundColor,
            onPress: () {
              print('click Google');
            },
          ),
        ),
      ],
    );
  }

}
