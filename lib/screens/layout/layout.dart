import 'package:blog/utils/dialog_builder.dart';
import 'package:blog/widgets/items/upload-image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blog/screens/home/home.dart';
import 'package:blog/styles/colors.dart';

class DashboardHomePage extends StatefulWidget {

  DashboardHomePage({ Key key}) : super(key: key);

  _DashboardHomePageState createState() => _DashboardHomePageState();

}

class _DashboardHomePageState extends State<DashboardHomePage> {

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(2),
        child: FloatingActionButton(
          onPressed: () {
            // DialogBuilder.buildLoadingDialog();
            // Future.delayed(Duration(milliseconds: 5000)).then((value) {
            //   Navigator.of(context).pop();
            // });
            DialogBuilder.buildNotificationDialog(
              barrierDismissible: true,
              message: 'Message',
              dialogType: DialogType.CONFIRM_CANCEL,
              urlImage: 'assets/images/system/ic_question.svg',

            );
          },
          elevation: 4,
          shape: CircleBorder(
            side: BorderSide(color: CommonColor.colorWhiteCard, width: 2,),
          ),
          child: Icon(Icons.add, color: CommonColor.colorWhiteCard,),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        elevation: 4,
        child: SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildBottomNavBarItem(index: 0, iconDisable: Icons.home_outlined, iconActive: Icons.home),
              _buildBottomNavBarItem(index: 1, iconDisable: Icons.video_collection_outlined, iconActive: Icons.video_collection),
              const SizedBox(width: 48,),
              _buildBottomNavBarItem(index: 2, iconDisable: Icons.message_outlined, iconActive: Icons.message),
              _buildBottomNavBarItem(index: 3, iconDisable: Icons.person_outline, iconActive: Icons.person),
            ],
          ),
        )
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBottomNavBarItem({ int index, IconData iconDisable, IconData iconActive }) {
    if (_currentIndex == index) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Icon(iconActive, size: 22, color: Theme.of(context).primaryColor,),
      );
    }
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Icon(iconDisable, size: 22, color: Theme.of(context).disabledColor,),
      ),
      onTap: () {
        setState(() {
          _currentIndex = index;
        });
      },
    );
  }


  // BottomNavigationBar _buildBottomNavigationBar() {
  //   return BottomNavigationBar(
  //     currentIndex: _currentIndex,
  //     elevation: 12,
  //     type: BottomNavigationBarType.fixed,
  //     backgroundColor: Colors.white,
  //     unselectedItemColor: Colors.grey,
  //     selectedItemColor: Colors.purple,
  //     items: [
  //       _buildBottomNavigationBarItem(icon: Icons.home, label: 'Home'),
  //       _buildBottomNavigationBarItem(icon: Icons.message, label: 'Message'),
  //       _buildBottomNavigationBarItem(icon: Icons.phone, label: 'Phone'),
  //       _buildBottomNavigationBarItem(icon: Icons.settings, label: 'Setting')
  //     ],
  //     onTap: (index) {
  //       setState(() {
  //         _currentIndex = index;
  //       });
  //     },
  //   );
  // }

  // BottomNavigationBarItem _buildBottomNavigationBarItem({ IconData icon, String label }) {
  //   return BottomNavigationBarItem(
  //     icon: Icon(icon),
  //     label: label
  //   );
  // }

  Widget _buildBody() {
    switch (_currentIndex) {
      case 0:
        return HomePage();
      case 1:
        return SizedBox();
      case 2:
        return SizedBox();
      case 3:
        return SizedBox();
      default:
        return SizedBox();
    }
  }

  // _buildCustomNavigationBar() {
  //   return Stack(
  //     children: [
  //       Positioned(
  //         bottom: 0,
  //         child: ClipPath(
  //           clipper: NavBarClipper(),
  //           child: Container(
  //             height: 60,
  //             width: MediaQuery.of(context).size.width,
  //             decoration: BoxDecoration(
  //               gradient: LinearGradient(
  //                 colors: [
  //                   Color(0xFF355c7d),
  //                   Color(0xFF6c5b7b),
  //                   Color(0xFFc06c84),
  //                 ],
  //                 begin: Alignment.bottomCenter,
  //                 end: Alignment.topCenter,
  //               )
  //             ),
  //           ),
  //         ),
  //       ),
  //     ],
  //   );
  // }

}
