import 'package:blog/config/regex.dart';
import 'package:blog/router/routing_name.dart';
import 'package:blog/styles/text_style.dart';
import  'package:blog/widgets/backgrounds/background.dart';
import 'package:blog/widgets/backgrounds/background_gradient.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginState createState() => _LoginState();

}

class _LoginState extends State<LoginScreen> {

  GlobalKey<FormState> _globalKeyLogin = new GlobalKey();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  bool _isPassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size _size =  MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          const Positioned(child: Background(), right: 0,),
          const Positioned(left: 0, bottom: 0, child: BackgroundGradient(),),
          Container(
            height: _size.height,
            width: _size.width,
            padding: const EdgeInsets.only(top: 120, left: 60, right: 60, bottom: 60,),
            child: SingleChildScrollView(
              child: _buildBodyLogin(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBodyLogin() {
    return Form(
      key: _globalKeyLogin,
      onChanged: () {
        print('onChange');
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset('assets/images/logo/logo_gradient.png', width: 80, height: 80,),
          const SizedBox(height: 20,),
          Text(
            'Welcome to GreFire',
            style: CommonTextStyle.size20Light.copyWith(color: Theme.of(context).primaryColor, fontSize: 24,),
          ),
          Text(
            'Login',
            style: CommonTextStyle.size20Bold.copyWith(color: Theme.of(context).primaryColor, fontSize: 24,),
          ),
          const SizedBox(height: 30,),
          BasicTextField(
            hintText: 'Email',
            regexConfig: RegexConstant.email,
            maxLength: 100,
            textInputFormatter: r'[^\s]',
          ),
          const SizedBox(height: 15,),
          BasicTextField(
            hintText: 'Password',
            regexConfig: RegexConstant.password,
            maxLength: 50,
            suffixIcon: GestureDetector(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                child: _isPassword
                  ? Icon(Icons.remove_red_eye_outlined, size: 16, color: Theme.of(context).disabledColor,)
                  : Icon(Icons.remove_red_eye, size: 16, color: Theme.of(context).primaryColor,)
              ),
              onTap: () {
                setState(() {
                  _isPassword = !_isPassword;
                });
              },
            ),
            textInputFormatter: r'[^\s]',
            isPassword: _isPassword,
          ),
          const SizedBox(height: 30,),
          RaisedButton(
            onPressed: () {
              _handleLogin(context);
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(
                'Login',
                textAlign: TextAlign.center,
                style: CommonTextStyle.size16Medium.copyWith(color: Theme.of(context).cardColor,),
              ),
            ),
          ),
          _buildOtherOptions(),
          const SizedBox(height: 20,),
        ],
      ),
    );
  }

  Widget _buildOtherOptions() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: InkWell(
            onTap: () {
              print('Click Forgot password?');
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10,),
              child: Text(
                'Forgot password?',
                style: CommonTextStyle.size14Medium.copyWith(color: Theme.of(context).primaryColor,),
              ),
            ),
          ),
        ),
        InkWell(
          onTap: () {
            print('Click Register');
            Navigator.of(context).pushNamed(RoutingNameConstant.registerScreen);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10,),
            child: Text(
              'Register',
              textAlign: TextAlign.right,
              style: CommonTextStyle.size14Medium.copyWith(color: Theme.of(context).primaryColor,),
            ),
          ),
        ),
      ],
    );
  }

  void _handleLogin(BuildContext context) {
    if (!_globalKeyLogin.currentState.validate()) return;
    print('login');
    Navigator.of(context).pushNamedAndRemoveUntil(RoutingNameConstant.homeScreen, (route) => false);
  }

}