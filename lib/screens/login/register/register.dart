import 'package:blog/config/regex.dart';
import 'package:blog/styles/text_style.dart';
import 'package:blog/utils/appbar_builder.dart';
import 'package:blog/widgets/backgrounds/background.dart';
import 'package:blog/widgets/backgrounds/background_gradient.dart';
import 'package:blog/widgets/inputs/basic_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {

  @override
  _RegisterState createState() => _RegisterState();

}

class _RegisterState extends State<RegisterScreen> {

  GlobalKey<FormState> _globalKeyRegister = new GlobalKey();
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  TextEditingController _confirmPassController = new TextEditingController();

  @override


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const Positioned(child: Background(), right: 0,),
          const Positioned(left: 0, bottom: 0, child: BackgroundGradient(),),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(top: 30, left: 60, right: 60, bottom: 30,),
            child: Center(
              child: SingleChildScrollView(
                child: _buildBody(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _confirmPassController.dispose();
    _emailController.dispose();
    _passController.dispose();
    _usernameController.dispose();
    super.dispose();
  }

  Widget _buildBody() {
    return Form(
      key: _globalKeyRegister,
      onChanged: () {
        print('onChange');
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset('assets/images/logo/logo_gradient.png', width: 80, height: 80,),
          const SizedBox(height: 20,),
          Text(
            'Welcome to GreFire',
            style: CommonTextStyle.size20Light.copyWith(color: Theme.of(context).primaryColor, fontSize: 24,),
          ),
          Text(
            'Register',
            style: CommonTextStyle.size20Bold.copyWith(color: Theme.of(context).primaryColor, fontSize: 24,),
          ),
          const SizedBox(height: 30,),
          BasicTextField(
            hintText: 'Username',
            regexConfig: RegexConstant.username,
            maxLength: 100,
            textInputFormatter: r'[\w-]',
          ),
          const SizedBox(height: 15,),
          BasicTextField(
            hintText: 'Email',
            regexConfig: RegexConstant.email,
            maxLength: 100,
            textInputFormatter: r'[^\s]',
          ),
          const SizedBox(height: 15,),
          BasicTextField(
            hintText: 'Password',
            regexConfig: RegexConstant.password,
            maxLength: 50,
            isPassword: true,
            textInputFormatter: r'[^\s]',
          ),
          const SizedBox(height: 15,),
          BasicTextField(
            hintText: 'Confirm password',
            regexConfig: RegexConstant.password,
            maxLength: 50,
            isPassword: true,
            textInputFormatter: r'[^\s]',
          ),
          const SizedBox(height: 30,),
          RaisedButton(
            onPressed: () {
              _handleRegister();
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(
                'Register',
                textAlign: TextAlign.center,
                style: CommonTextStyle.size16Medium.copyWith(color: Theme.of(context).cardColor,),
              ),
            ),
          ),
          const SizedBox(height: 5,),
          Align(
            alignment: Alignment.center,
            child: GestureDetector(
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                color: Colors.transparent,
                child: Text(
                  'Back to Login',
                  style: CommonTextStyle.size14Medium.copyWith(color: Theme.of(context).primaryColor,),
                ),
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ),
        ],
      ),
    );
  }

  void _handleRegister() {
    if (!_globalKeyRegister.currentState.validate()) return;
    print('handle register');
  }

}
