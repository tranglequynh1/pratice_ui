import 'package:blog/plugin/locator.dart';
import 'package:blog/plugin/navigator.dart';
import 'package:blog/styles/text_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final currentContext = locator<NavigatorService>().navigatorKey.currentState.overlay.context;

void uploadImage () {
  showModalBottomSheet(
    context: currentContext,
    builder: (context) {
      return Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 120, vertical: 10,),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(3),
                child: Divider(height: 3, thickness: 3, color: Theme.of(currentContext).hintColor,),
              ),
            ),
            buildItem(
              title: 'Gallery',
              icon: SvgPicture.asset(
                'assets/images/system/ic_image.svg',
                fit: BoxFit.contain,
              ),
              function: null,
            ),
            Divider(height: 2,),
            buildItem(
              title: 'Camera',
              icon: SvgPicture.asset(
                'assets/images/system/ic_camera.svg',
                fit: BoxFit.contain,
              ),
              function: null,
            ),
            Divider(height: 2,),
            buildItem(
              title: 'Cancel',
              icon: Center(
                child: SvgPicture.asset(
                  'assets/images/system/ic_cancel_line.svg',
                  height: 24,
                  width: 24,
                ),
              ),
              function: () {
                Navigator.of(currentContext).pop();
              },
            ),
          ],
        ),
      );
    }
  );
}

Widget buildItem({ String title, VoidCallback function, Widget icon }) {
  return GestureDetector(
    child: Container(
      width: MediaQuery.of(currentContext).size.width,
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      color: Colors.transparent,
      child: Row(
        children: [
          const SizedBox(width: 10,),
          SizedBox(
            height: 36,
            width: 36,
            child: icon,
          ),
          const SizedBox(width: 20,),
          Text(
            title ?? '',
            style: CommonTextStyle.size16Medium.copyWith(color: Theme.of(currentContext).canvasColor,),
          ),
          const SizedBox(width: 10,),
        ],
      ),
    ),
    onTap: function,
  );
}
