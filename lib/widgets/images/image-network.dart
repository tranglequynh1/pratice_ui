import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

Widget imageNetwork({ @required String url, BoxFit boxFit }) {
  return Image.network(
    url,
    fit: boxFit ?? BoxFit.contain,
    alignment: Alignment.center,
    loadingBuilder: (context, child, loadingProgress) {
      if (loadingProgress == null) return child;
      return SizedBox(
        width: 32,
        height: 32,
        child: CircularProgressIndicator(strokeWidth: 2,),
      );
    },
    errorBuilder: (context, error, stackTrace) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: SvgPicture.asset('assets/images/system/ic_image.svg'),
      );
    },
  );
}