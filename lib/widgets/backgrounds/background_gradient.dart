import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BackgroundGradient extends StatelessWidget {

  const BackgroundGradient();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomLeft,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.transparent,
            Colors.transparent,
            Theme.of(context).accentColor.withOpacity(0.01),
            Theme.of(context).primaryColor.withOpacity(0.38),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          tileMode: TileMode.mirror,
        ),
      ),
    );
  }
}
