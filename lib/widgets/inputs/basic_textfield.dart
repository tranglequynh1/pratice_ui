import 'package:blog/models/common/regex_config.dart';
import 'package:blog/styles/text_style.dart';
import 'package:blog/utils/input_formatters.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BasicTextField extends StatelessWidget {

  final TextEditingController controller;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final bool enabled;
  final int maxLength;
  final int maxLines;
  final String textInputFormatter;
  final String hintText;
  final RegexConfig regexConfig;
  final bool isPassword;

  BasicTextField({
    this.controller,
    this.prefixIcon,
    this.suffixIcon,
    this.enabled = true,
    this.maxLength = 256,
    this.hintText,
    this.maxLines = 1,
    this.textInputFormatter = r'.',
    @required this.regexConfig,
    this.isPassword = false,

  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: Theme.of(context).accentColor,
      textInputAction: TextInputAction.done,
      obscureText: this.isPassword,
      style: CommonTextStyle.size16Light,
      textAlignVertical: TextAlignVertical.center,
      textAlign: TextAlign.left,
      validator: (value) {
        value = value.trim();
        if (!regexConfig.getRegExp.hasMatch(value)) {
          return regexConfig?.errorText;
        }
        return null;
      },
      inputFormatters: [
        LengthLimitation(maxLength: maxLength),
        FilteringTextInputFormatter.allow(RegExp(textInputFormatter)),
      ],
      decoration: InputDecoration(
        filled: false,
        hintText: this.hintText,
        contentPadding: const EdgeInsets.symmetric(horizontal: 3, vertical: 6,),
        disabledBorder: _setInputBorder(color: Theme.of(context).disabledColor.withOpacity(0.8)),
        enabledBorder: _setInputBorder(color: Theme.of(context).primaryColor.withOpacity(0.3)),
        focusedBorder: _setInputBorder(color: Theme.of(context).primaryColor.withOpacity(0.8)),
        errorBorder: _setInputBorder(color: Theme.of(context).errorColor.withOpacity(0.6)),
        focusedErrorBorder: _setInputBorder(color: Theme.of(context).errorColor),
        border: _setInputBorder(color: Colors.yellow),
        suffixIcon: this.suffixIcon,
        prefixIcon: this.prefixIcon,
        isDense: true,
        suffixIconConstraints: BoxConstraints(
          minHeight: 28,
        ),
        prefixIconConstraints: BoxConstraints(
          minHeight: 28,
        ),
        hintStyle: CommonTextStyle.size14Light.copyWith(color: Theme.of(context).disabledColor.withOpacity(0.5)),
        errorStyle: CommonTextStyle.size12Light.copyWith(color: Theme.of(context).errorColor, fontStyle: FontStyle.italic),
      ),
    );
  }

  _setInputBorder({ Color color, double width }) {
    return UnderlineInputBorder(
      borderSide: BorderSide( width: width ?? 1, color: color, style: BorderStyle.solid),
    );
  }

}