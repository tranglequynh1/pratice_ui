import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:blog/styles/text_style.dart';

Widget flatButton({@required String title, String image, Color textColor, Color borderColor, Color fillColor, VoidCallback onPress}) {
  return FlatButton(
    onPressed: onPress,
    splashColor: borderColor.withOpacity(0.1),
    color: fillColor ?? Colors.transparent,
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15,),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        image == null
          ? SizedBox()
          : Padding(
              padding: const EdgeInsets.only(
                right: 8,
              ),
              child: SvgPicture.asset(
                image,
                fit: BoxFit.contain,
                height: 20,
              ),
            ),
        Text(
          title ?? '',
          style: CommonTextStyle.size16Medium.copyWith(
            color: textColor,
          ),
        ),
      ],
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(25),
      side: BorderSide(
        width: 0.8,
        color: borderColor,
        style: BorderStyle.solid,
      ),
    ),
  );
}
