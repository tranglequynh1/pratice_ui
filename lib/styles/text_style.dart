import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:blog/styles/colors.dart';

abstract class CommonTextStyle {

  static FontWeight light = FontWeight.w300;
  static FontWeight medium = FontWeight.w400;
  static FontWeight bold = FontWeight.w700;

  static TextStyle size12Light = TextStyle(
    fontSize: 12,
    fontWeight: light,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size12Medium = TextStyle(
    fontSize: 12,
    fontWeight: medium,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size12Bold = TextStyle(
    fontSize: 12,
    fontWeight: bold,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size14Light = TextStyle(
    fontSize: 14,
    fontWeight: light,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size14Medium = TextStyle(
    fontSize: 14,
    fontWeight: medium,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size14Bold = TextStyle(
    fontSize: 14,
    fontWeight: bold,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size16Light = TextStyle(
    fontSize: 16,
    fontWeight: light,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size16Medium = TextStyle(
    fontSize: 16,
    fontWeight: medium,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size16Bold = TextStyle(
    fontSize: 16,
    fontWeight: bold,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size18Light = TextStyle(
    fontSize: 18,
    fontWeight: light,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size18Medium = TextStyle(
    fontSize: 18,
    fontWeight: medium,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size18Bold = TextStyle(
    fontSize: 18,
    fontWeight: bold,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size20Light = TextStyle(
    fontSize: 20,
    fontWeight: light,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size20Medium = TextStyle(
    fontSize: 20,
    fontWeight: medium,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

  static TextStyle size20Bold = TextStyle(
    fontSize: 20,
    fontWeight: bold,
    color: CommonColor.colorBlack,
    decoration: TextDecoration.none,
  );

}