import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

abstract class CommonColor {
  static Color colorBaseGreen = Color(0xFF3ED3A3);
  static Color colorGreen = Color(0xFF02B2B9);
  static Color colorGreenDark = Color(0xFF028B91);
  static Color colorGreenPastel = Color(0xFFD6F1F2);
  static Color colorYellow = Color(0xFFFFCF00);
  static Color colorLight = Color(0xFFFFCF00);
  static Color colorYellowDark = Color(0xFFE9AC05);
  static Color colorRed = Color(0xFFFE6767);
  static Color colorRedPastel = Color(0xFFF5BCC7);
  static Color colorBlack = Color(0xFF2C2E31);
  static Color colorGrey = Color(0xFF9B9FA4);
  static Color colorGreyPastel = Color(0xFFDFE1E2);
  static Color colorWhiteBg = Color(0xFFF6F6F6);
  static Color colorWhiteCard = Color(0xFFFBFBFC);

}

class MaterialColors {
  MaterialColors();

  static const _primaryLight = 0xFF02B2B9;
  static const _primaryDark = 0xFF12128A;

  static const MaterialColor colorLight = const MaterialColor(
    _primaryLight,
    const <int, Color>{
      50:  const Color(0xFFFBFBFC),
      100: const Color(0xFFF6F6F6),
      200: const Color(0xFF02B2B9),
      300: const Color(0xFF028B91),
      400: const Color(0xFF02B2B9),
      500: const Color(0xFFFFCF00),
      600: const Color(0xFF028B91),
      700: const Color(0xFF2C2E31),
      800: const Color(0xFF000000),
      900: const Color(0xFF000000),
    },
  );
}
