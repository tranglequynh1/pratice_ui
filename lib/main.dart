import 'package:blog/plugin/locator.dart';
import 'package:blog/plugin/navigator.dart';
import 'package:blog/router/router.dart';
import 'package:blog/screens/home/home.dart';
import 'package:blog/screens/layout/layout.dart';
import 'package:flutter/material.dart';
import 'package:blog/providers/common/themes_provider.dart';
import 'package:blog/screens/splash/splash.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ThemesManager()),
      ],
      child: MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemesManager>(
      builder: (context, themes, child) {
        return MaterialApp(
          title: 'Practice UI',
          theme: themes.themeData,
          home: DashboardHomePage(),
          navigatorKey: locator<NavigatorService>().navigatorKey,
          routes: RouterConstant.routes,
          debugShowCheckedModeBanner: false,
        );
      },
    );
  }
}