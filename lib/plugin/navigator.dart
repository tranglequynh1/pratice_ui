import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavigatorService {

  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  NavigatorService() {
    print('init navigator service');
  }

  Future<dynamic> pushNamed(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }

  Future<dynamic> pushReplacementNamed(String routeName) {
    return navigatorKey.currentState.pushReplacementNamed(routeName);
  }

  Future<dynamic> pushNamedAndRemoveUntil(String routeName) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName, (_) => false);
  }

  void pop() {
    return navigatorKey.currentState.pop();
  }



}

